from pyfaces import pyfaces
import sys,time
import paho.mqtt.client as mqtt
import threading
import json
import requests

global debug
debug = False
global mqttCommand
mqttCommand = ""

global server_name
server_name = "stellarbd.com"
global unit_id
unit_id = "EM9409358494"
global main_topic
main_topic = "rams_test"

#Command Flag
global updateRegistry
global memoryResetFlag
global newMessageFlag
global newUserFlag
global sendRecordSizeFlag
global sendAllUserFlag
global userInfoFlag
global registrationMode
global mqttLiveDataFlag
global httpLiveDataSendFlag


updateRegistry = False
memoryResetFlag = False
newMessageFlag = False
newUserFlag = False
sendRecordSizeFlag = False
sendAllUserFlag = False
userInfoFlag = False
registrationMode = False
mqttLiveDataFlag = False
httpLiveDataSendFlag = False


def on_connect(client, userdata, flags, rc):
  topic_name = 'control_instruction'
  subsciptionUnit = main_topic+'/'+unit_id+'/'+topic_name
  client.subscribe(subsciptionUnit)

def on_message(client, userdata, msg):
  global mqttCommand
  print msg.payload
  mqttCommand = msg.payload
  #client.disconnect()
  checkFlag()

def Mqtt() :

    client = mqtt.Client()
    client.connect(server_name, 1883, 60)
    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_forever()

def CommandProcessing():
    global registrationMode
    global updateRegistry
    
    while 1:
        if newUserFlag :
            print

        if updateRegistry:
            PyFaces.updateRegistryModeProcedure()

        if registrationMode : #if registrationMode and regdoneFlagInCamera
            data = PyFaces.registratoModeProcedure()
            
            #Need to be tested
            JSON = {'unit_id':unit_id,'card_finger':data}
            jsonData = json.dumps(JSON)
            #print 'dumped json: ', jsonData
            print 'data : ', data

            client = mqtt.Client()
            client.connect(server_name,1883,60)

            topic = "template"
            subsciptionUnit = main_topic+'/'+unit_id+'/'+topic
            client.publish(subsciptionUnit,data);

            registrationMode = False

            #End registration mode request from device
            #"http://stellarbd.com/rams_test/default/time_out?unit_id=EM9409358494"
            url = 'http://'+server_name+ '/'+main_topic+'/'+'default/time_out?unit_id='+unit_id
            response = requests.post(url)
            print(response.status_code, response.reason) 


    
def checkFlag() :
    #global mqttCommand
    global registrationMode
    global updateRegistry

    print mqttCommand[0], mqttCommand[1], mqttCommand[2], mqttCommand[3]

    if ( mqttCommand[0] == '1' ):
        print "Biling True ", mqttCommand[0]    

    if ( mqttCommand[1] == '1' ):
        print "Update Registry True ", mqttCommand[1]  
        updateRegistry = True

    if ( mqttCommand[2] == '1' ):
        print "MQTT Live Debug True ", mqttCommand[2]

    if ( mqttCommand[3] == '1' ):
        print "Memory Reset On ", mqttCommand[3]
        #memoryResetFlag = True

    if ( mqttCommand[4] == '1' ):
        print "New Message Fug True ", mqttCommand[4]
	#newMessageFlag = True

    if ( mqttCommand[5] == '1' ):
        print "New User True", mqttCommand[5]
        newUserFlag = True

    if ( mqttCommand[6] == '1' ):
        print "Restart Device True ", mqttCommand[6]

    if ( mqttCommand[7] == '1' ):
        print "Send Record Size True ", mqttCommand[7]
        #sendRecordSizeFlag = True

    if ( mqttCommand[8] == '1' ):
        print "Send All Users True ", mqttCommand[8]
        sendAllUserFlag = True
     
    if ( mqttCommand[9] == '1' ):
        print "User Info Flag True ", mqttCommand[9]
        #userInfoFlag = True

    if ( mqttCommand[10] == '1' ):
        print "User Log Flag True ", mqttCommand[10]

    if ( mqttCommand[11] == '1' ):
        print "User Firmware True ", mqttCommand[11]

    if ( mqttCommand[12] == '1' ):
        registrationMode = True
        print "Registration Mode True ", mqttCommand[12]

    if ( mqttCommand[13] == '1' ):
        print "Mqtt Live Data True ", mqttCommand[13]
        #mqttLiveDataFlag = True

    if ( mqttCommand[14] == '1' ):
        print "Mqtt SetUp Wifi True ", mqttCommand[14]

    if ( mqttCommand[15] == '1' ):
        print "Regular Alarm True ", mqttCommand[15]

    if ( mqttCommand[16] == '1' ):
        print "Special Alarm True ", mqttCommand[16]

    if ( mqttCommand[17] == '1' ):
        print "Instant Alarm True ", mqttCommand[17]

    if ( mqttCommand[18] == '1' ):
        print "Late Send Log True ", mqttCommand[18]

    if ( mqttCommand[19] == '1' ):
        print "HTTP Live Data Send True ", mqttCommand[19]
        #httpLiveDataSendFlag = True

if __name__ == "__main__":
    try:
        start = time.time()

        PyFaces = pyfaces.PyFacesForGeneralPurpose()
        mqtttThread = threading.Thread(target = Mqtt)
        mqtttThread.start()
       

	print "--------------------------------------------------------------------------------"
        argsnum=len(sys.argv)
        print "Args:",argsnum

	#5 = 18,  23=38 for average images distortion
    	picturename = '20s1.pgm'
	imgname = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/test31x31/'+picturename
	#dirname = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/database31x38/'
    	#dirname = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/avgimages31x31' 
        dirname = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/database31x31'  
	#1-5, 3 - 23, 4-40,5-15,18-40

	egfaces = 0
	thrshld = 600000000000000000000000000000000000000
	print "Image Name : " +imgname
	
        pyf=pyfaces.PyFaces(imgname,dirname,egfaces,thrshld)
       

        end = time.time()
        print 'Took :',(end-start),'Sec'

        while True:
            CommandProcessing()

	print "--------------------------------------------------------------------------------"

    except Exception,detail:
        print detail.args
        print "usage:python pyfacesdemo imgname dirname numofeigenfaces threshold "
