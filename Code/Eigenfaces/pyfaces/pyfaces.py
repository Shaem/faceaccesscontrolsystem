import  sys
from string import split
from os.path import basename
import eigenfaces
import os
import requests
import json
global DEBUG 
DEBUG = False
global server_name
server_name = "stellarbd.com"
global unit_id
unit_id = "EM9409358494"
global main_topic
main_topic = "rams_test"

Facet = eigenfaces.FaceRec()


class PyFaces:
    def __init__(self,testimg,imgsdir,egfnum,thrsh):
	print "Pyfaces __init__ starts ....." if DEBUG  else 0
        self.testimg=testimg
        self.imgsdir=imgsdir
        self.threshold=thrsh
        self.egfnum=egfnum        
        parts = split(basename(testimg),'.')
        extn=parts[len(parts) - 1]


        if DEBUG: print 'Parts : ', parts, "length : ", len(parts) 
        if DEBUG: print 'Basename : ', basename(testimg) 
        if DEBUG: print 'Extention : ', extn, 'Type : ', type(extn) 
	if DEBUG: print "Searching In : ", imgsdir

        self.facet= eigenfaces.FaceRec()
        self.egfnum=self.set_selected_eigenfaces_count(self.egfnum,extn)

        if DEBUG: print "Number of Eigenfaces used:",self.egfnum 

        self.facet.checkCache(self.imgsdir,extn,self.imgnamelist,self.egfnum,self.threshold)
       
	if DEBUG: print "Egfnum : ", self.egfnum, "  Threshold : ", self.threshold 
        mindist,matchfile=self.facet.findmatchingimage(self.testimg,self.egfnum,self.threshold)


        if mindist < 1e-10:
            mindist=0
        if not matchfile:
            print "NOMATCH! try higher threshold"
        else:
	    # print "******************************************************************"
	    print
            print
	    print "Images : "+ str(self.testimg)
            print "Matches :"+matchfile # +" dist :"+str(mindist)
	    # print "******************************************************************"
            print "Dist Value : "+str(mindist)
        

	if DEBUG: print "Pyfaces __init__ ends ..." 
            
    def set_selected_eigenfaces_count(self,selected_eigenfaces_count,ext):        
        #call eigenfaces.parsefolder() and get imagenamelist        
        self.imgnamelist=self.facet.parsefolder(self.imgsdir,ext)                    
        numimgs=len(self.imgnamelist)        
        if(selected_eigenfaces_count >= numimgs  or selected_eigenfaces_count == 0):
            selected_eigenfaces_count=numimgs/2    
        return selected_eigenfaces_count


class PyFacesForGeneralPurpose(object):

    def updateRegistryModeProcedure( self ):
        #https://stellarbd.com/rams_test/default/send_temp_buffer_data_2.json?unit_id=EM9409358494'
        url = 'https://'+server_name+'/'+ main_topic +'/' +'default/send_temp_buffer_data_2.json?unit_id='+ unit_id
        response = requests.get(url)
        JSON = json.loads(response.content)

        checksum = JSON['checksum']
        operation = JSON['operation']
        roll = JSON['roll']
        signature = JSON['signature']
        
        print 'checksum : ', checksum
        print 'operation : ', operation
        print 'rol : ', roll
        print 'signature : ', signature , ' Data Type: ', type(signature)
        
        #Will give error as json are in list data type
        if(int(operation) == 1): #if operation == 1 i.e ADD Instruction
            numimgs = 5
            numpixels = 961
            imgname = roll
            Facet.savingImagesFromStringData(numimgs, numpixels, imgname, signature)

        if(int(operation) == 5): #if operation == 5 i.e DELETE Instruction
            #Image should be deleted
            for i in range(20):
                imagename = roll+'s'+str(i)+'.pgm'
                directory = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/reconfaces/'
                name = directory+imagename
                count = 0
                if os.path.exists(name):
                    os.remove(name)
                    count += 1
                print count, ' File Removed'




    def registratoModeProcedure( self ):

        regImgNumbers = 5
        directory = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/reconfaces/'
        ext = "pgm"
        data = Facet.addImageDataFromDir(regImgNumbers, directory, ext)
        if DEBUG: print 'data', data
        if DEBUG: print 'Pyfaces Registration  Mode End'
        return data
        
