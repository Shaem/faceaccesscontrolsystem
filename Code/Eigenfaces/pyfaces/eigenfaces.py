from numpy import max
from numpy import zeros
from numpy import average
from numpy import dot
from numpy import asfarray
from numpy import sort
from numpy import trace
from numpy import argmin
from numpy.linalg import *
from os.path import isdir,join,normpath
from os import listdir,mkdir
from shutil import rmtree
from math import sqrt
import imageops
import pickle
import cPickle
import time
import MySQLdb
import decimal
import numpy as np
import json
global DEBUG

DEBUG = False
#DEBUG = True

global imgWidth
global imgHight
imgWidth = 31
imgHight = 31
global recondir
recondir = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/reconfaces/'
avgimgdir = '/home/shaem/Git/faceaccesscontrolsystem/Code/Eigenfaces/avgimages31x31/'

# Connecting with database 
db=MySQLdb.connect(host="localhost",user="root",passwd="rumy",db="opencv")
cursor=db.cursor()

class ImageError(Exception):
    pass

class DirError(Exception):
    pass 
class FaceBundle:
    def __init__(self,imglist,wd,ht,adjfaces,fspace,avgvals,evals):
        self.imglist=imglist
        self.wd=wd
        self.ht=ht
        self.adjfaces=adjfaces
        self.eigenfaces=fspace
        self.avgvals=avgvals
        self.evals=evals
        
class FaceRec:    
    def validateselectedimage(self,imgname):                     
        selectimg=imageops.XImage(imgname)
	if DEBUG: print "Selectimg in Validateselectedimage : ", selectimg 
        selectwdth=selectimg._width
        selectht=selectimg._height        
        if((selectwdth!=self.bundle.wd) or (selectht!=self.bundle.ht)):
            raise ImageError("Please select image of correct size !")
        else:
            return selectimg

    def addSignatures(self, imageArrayData):
        imageData = ""

        for i in imageArrayData:
            if DEBUG: print 'ImageData : ',self.floatArrayToHexString(i)
            imageData += self.floatArrayToHexString(i)
    
        return imageData


    def floatArrayToHexString(self,floatArray):
        print
        List = []
        string = ""
        #float to Hex Convertion
        heximage = ['{:02x}'.format(int(i)) for i in floatArray]

        #Hexarray to HexString
        for i in heximage:
            string = string+i
        return string

    def hexStringToFloatArray(self,hexString):

        floatlist = []
        parse = ""
        j = 0
        #HexString  to float List 
        for i in hexString:
            parse = parse + i
            if(j%2 != 0):
                floatdata = int(parse,16)
                floatlist.append(float(floatdata))
                parse = ""
            j+=1
        
        #Converting Floarlist to float array
        floatArray = np.asarray(floatlist)
        return floatArray
    def constructImageFromArray(self, imageName, array):

        imgname=recondir+"/reconphi"+str(imageName) +".pgm" 
        imgdata=array
        imageops.make_image(imgdata,imgname,(imgWidth,imgHight),True)

    def savingImagesFromStringData(self, numImages, numPixels, imgName, data):
        numPixels *= 2 #As it is string
        stripfrom = 0
        stripto = numPixels

        for i in range(numImages):
            str = data[ stripfrom : stripto] 
            if DEBUG: print 'str',i,' : ',str
            stripfrom += numPixels
            stripto = stripfrom +numPixels
            imageName = imgName + str(i)
            self.constructImageFromArray(imageName, self.hexStringToFloatArray(str))
   
    def findmatchingimage(self,imagename,selectedfacesnum,thresholdvalue):    
	if DEBUG: print "<... FindmatchingImage starts ....>" 

	if DEBUG: print "Input Image Name : " , imagename 
        selectimg=self.validateselectedimage(imagename)  #check whether the image is in correct size or not
        inputfacepixels=selectimg._pixellist

        inputFaceData=np.array(inputfacepixels)
        inputFaceData.astype(float)

        InputFaceString = self.floatArrayToHexString(inputFaceData)
        inputface = self.hexStringToFloatArray(InputFaceString)

        #Procedure of constructing an image from array
        '''
        recondir='../reconfaces'
        if isdir(recondir):                             
            rmtree(recondir,True)
        mkdir(recondir)
        print "Made:",recondir

        imgname=recondir+"/reconphi"+"TEST"+".png" 
        imgdata=inputface         
        imageops.make_image(imgdata,imgname,(self.bundle.wd,self.bundle.ht),True)
        '''
        
        pixlistmax=max(inputface)
        inputfacen=inputface/pixlistmax        

	if DEBUG: print "InputFacen : ", inputfacen 
	if DEBUG: print "Length of InputFacen : ", len(inputfacen) 
	
	
        inputface=inputfacen-self.bundle.avgvals
	if DEBUG: print "Inputface = ", inputface 
	if DEBUG: print "Self.bundle.avgvals : " , self.bundle.avgvals 
	
	

        usub=self.bundle.eigenfaces[:selectedfacesnum,:]
	input_wk=dot(usub,inputface.transpose()).transpose() 
        dist = ((self.weights-input_wk)**2).sum(axis=1)
	if DEBUG: print "Dist : ", dist 
	if DEBUG: print "Length of Dist : ", len(dist)
	

        idx = argmin(dist)
	if DEBUG: print "IDX : ", idx 

        mindist=sqrt(dist[idx])
	if DEBUG: print "Mindist : ", mindist 
	

        result=""
        if mindist < thresholdvalue:
            result=self.bundle.imglist[idx]
        if DEBUG: print "Tring to reconstruct !!" 
        #self.reconstructfaces(selectedfacesnum)
	if DEBUG: print "Selectfacesnum : ", selectedfacesnum 
	if DEBUG: print "Result : " , result 
	if DEBUG: print "<... FindmatchingImage ENDS ....>" 
        return mindist,result
        

    def doCalculations(self,dir,imglist,selectednumeigenfaces): 
	if DEBUG: print "<... DoCalculations starts ....>" 

        #self.createAverageImages(imglist)
        
        self.createFaceBundle(imglist);        
        egfaces=self.bundle.eigenfaces
        adjfaces=self.bundle.adjfaces
        self.weights=self.calculateWeights(egfaces,adjfaces,selectednumeigenfaces)
        

	'''
	#Uncomment it if you want to write to cache
        #write to cache
	print "cachefile=join(dir,saveddata.cache)"
        cachefile=join(dir,"saveddata.cache")
	print "f2=open(cachefile,w)"
        f2=open(cachefile,"w")
	print "f2=open(cachefile,w)"
        pickle.dump(self.bundle,f2)
	print "<... DoCalculations ENDS ....>" 
        f2.close()
	'''
        
    def validateDirectory(self,imgfilenameslist):    
	if DEBUG: print "<... ValidateDirectory starts ....>"          
        if (len(imgfilenameslist)==0):
            print "Folder empty!"
            raise DirError("Folder empty***")
        imgfilelist=[]
        for z in imgfilenameslist:
            img=imageops.XImage(z)
            imgfilelist.append(img)        
        sampleimg=imgfilelist[0]
        imgwdth=sampleimg._width
        imght=sampleimg._height        
        #check if all images have same dimensions
        for x in imgfilelist:
            newwdth=x._width
            newht=x._height
            if((newwdth!=imgwdth) or (newht!=imght)):
                raise DirError("select folder with all images of equal dimensions !")
	print "<... VakidateDirectory ENDS ....>"   
        return imgfilelist
    
    def calculateWeights(self,eigenfaces,adjfaces,selectedfacesnum):  
	print "<... CalculateWeights starts ....>"   	
                     
        usub=eigenfaces[:selectedfacesnum,:]    
        wts=dot(usub,adjfaces.transpose()).transpose()  

	if DEBUG: print "<... CalculateWeights ENDS ....>"                 
        return wts           
            
    def addImageDataFromDir(self, numimages, directory, ext):

        imgfilenamelist = self.parsefolder(directory,ext)
        imgfilelist=self.validateDirectory(imgfilenamelist)
        img=imgfilelist[0]
        imgwdth=img._width
        imght=img._height
        numpixels=imgwdth * imght
        testfacemat = zeros((numimages,numpixels))
        for i in range(numimages):
            pixarray=asfarray(imgfilelist[i]._pixellist)
            testfacemat[i,:] = pixarray

        if DEBUG: print 'testfacemat : ', testfacemat
        stringData  = self.addSignatures(testfacemat)
        return stringData


    def createAverageImages(self,imglist):
        imgfilelist=self.validateDirectory(imglist)   
        print 'len of imgfilelist : ', len(imgfilelist)
        img=imgfilelist[0]
        imgwdth=img._width
        imght=img._height
        numpixels=imgwdth * imght
   	if DEBUG: print "Numpixels : ", numpixels
        numimgs=len(imgfilelist)               
        if DEBUG: print "NumImgs : " , numimgs
    
        #trying to create a 2d array ,each row holds pixvalues of a single image
        facemat=zeros((numimgs,numpixels))               

        for i in range(numimgs):
            pixarray=asfarray(imgfilelist[i]._pixellist)
            pixarraymax=max(pixarray)
            pixarrayn=pixarray/pixarraymax                
            facemat[i,:]=pixarrayn   
    
        avgvals=average(facemat,axis=0)    
        if DEBUG: print "Avgvals : ", avgvals
        if DEBUG: print "Lenght of Avgvals : ", len(avgvals)

        imgname=avgimgdir+'avg'+'40s' +".pgm" 
        imgdata=avgvals
        imageops.make_image(imgdata,imgname,(imgWidth,imgHight),True)




    # CreateFaceBundle
    def createFaceBundle(self,imglist):
	if DEBUG: print "<... createFaceBundle starts ....>" 

        imgfilelist=self.validateDirectory(imglist)   
        img=imgfilelist[0]
        imgwdth=img._width
        imght=img._height
        numpixels=imgwdth * imght
	if DEBUG: print "Numpixels : ", numpixels

        numimgs=len(imgfilelist)               
	if DEBUG: print "NumImgs : " , numimgs
	
        #trying to create a 2d array ,each row holds pixvalues of a single image
        facemat=zeros((numimgs,numpixels))               
        testfacemat = zeros((5,numpixels))

        for i in range(numimgs):
            pixarray=asfarray(imgfilelist[i]._pixellist)

            '''
            if(i<5):
                testfacemat[i,:] = pixarray
            '''
            pixarraymax=max(pixarray)
            pixarrayn=pixarray/pixarraymax                
            facemat[i,:]=pixarrayn   
        ''' 
        #Testing Purpose
        if DEBUG: print 'testfacemat : ', testfacemat
        stringData  = self.addSignatures(testfacemat)
        #print 'stringData : ', stringData
        self.savingImagesFromStringData(5,numpixels,stringData)
        '''


        #create average values ,one for each column(ie pixel)        
        avgvals=average(facemat,axis=0)    
	if DEBUG: print "Avgvals : ", avgvals
	if DEBUG: print "Lenght of Avgvals : ", len(avgvals)
  
        #make average faceimage in currentdir just for fun viewing..
        #imageops.make_image(avgvals,"average.png",(imgwdth,imght))      
         
        #substract avg val from each orig val to get adjusted faces(phi of T&P)     
        adjfaces=facemat-avgvals        
        if DEBUG: print "Adjfaces : ", adjfaces
	if DEBUG: print "Length of Adjfaces : ", len(adjfaces) 
        adjfaces_tr=adjfaces.transpose()
        L=dot(adjfaces , adjfaces_tr)
        evals1,evects1=eigh(L)

        #to use svd ,comment out the previous line and uncomment the next
        #evects1,evals1,vt=svd(L,0)        
        reversedevalueorder=evals1.argsort()[::-1]
        evects=evects1[:,reversedevalueorder]               
        evals=sort(evals1)[::-1]                
        #rows in u are eigenfaces        
        u=dot(adjfaces_tr,evects)
        u=u.transpose()               
        #NORMALISE rows of u
        for i in range(numimgs):
            ui=u[i]
            ui.shape=(imght,imgwdth)
            norm=trace(dot(ui.transpose(), ui))  
            u[i]=u[i]/norm  

        self.bundle=FaceBundle(imglist,imgwdth,imght,adjfaces,u,avgvals,evals)
        #self.createEigenimages(u)# eigenface images
	print "<... createFaceBundle ENDS ....>"  
	
        
    
    def parsefolder(self,dirname,extn):    
	if DEBUG: print "<... parsefolder starts ....>" 
	            
        if not isdir(dirname): return
        imgfilenameslist=sorted([
            normpath(join(dirname, fname))
            for fname in listdir(dirname)
            if fname.lower().endswith('.'+extn)            
            ])        	
        return imgfilenameslist
	if DEBUG: print "imgfilenamelist in parse folder : ", imgfilenameslist 
        if DEBUG: print "<... parsefolder ENDS ....>" 

    def reconstructfaces(self,selectedfacesnum): 
	if DEBUG: print "<... reconstructfaces starts ....>"  
	       
        #reconstruct                  
        recondir='../reconfaces'
        newwt=zeros(self.weights.shape)
        eigenfaces=self.bundle.eigenfaces
        usub=eigenfaces[:selectedfacesnum,:]
        evals=self.bundle.evals
        evalssub=evals[:selectedfacesnum]        
        for i in range(len(self.weights)):
            for j in range(len(evalssub)):        
                newwt[i][j]=self.weights[i][j]*evalssub[j]        
        phinew=dot(newwt,usub)    
        
        xnew=phinew+self.bundle.avgvals
        if isdir(recondir):                             
            rmtree(recondir,True)
        mkdir(recondir)
        print "Made:",recondir
        numimgs=len(self.bundle.imglist)
        for x in range(numimgs):
            imgname=recondir+"/reconphi"+str(x)+".png" 
            imgdata=phinew[x]           
            imageops.make_image(imgdata,imgname,(self.bundle.wd,self.bundle.ht),True)
            
        for x in range(numimgs):
            filename=recondir+"/reconx"+str(x)+".png"
            imgdata=xnew[x]
            imageops.make_image(imgdata,filename,(self.bundle.wd,self.bundle.ht),True)
	if DEBUG: print "<... reconstructfaces ENDS ....>" 

    
    def createEigenimages(self,eigenspace): 
	if DEBUG: print "<... createEigenimages starts ....>" 
               
        egndir='../eigenfaces'        
        if isdir(egndir):                
            rmtree(egndir,True)               
        mkdir(egndir)            
        numimgs=len(self.bundle.imglist)
        for x in range(numimgs):
            imgname=egndir+"/eigenface"+'1s' + str(x)+".pgm"            
            imageops.make_image(eigenspace[x],imgname,(self.bundle.wd,self.bundle.ht))
	if DEBUG: print "<... createEigenImages ENDS ....>" 
    
    def checkCache(self,dir,ext,imglist,selectedfacesnum,thrval):    
	print "<... checkcache starts ....>"   

	#Comment out it if you want to write to cache	
    	self.doCalculations(dir,imglist,selectedfacesnum)

	#Uncomment it if you want to write to cache
	#Uncomment another block in doCalculations()
	'''
        cachefile=join(dir,"saveddata.cache")
        cache_changed=True
        try:
	    print 
	    print "Overwritng Cache File "
	    self.doCalculations(dir,imglist,selectedfacesnum)
	    # print "f = open(catchfile) function is comment out"
            #f=open(cachefile)
        except IOError:
            print "no cache file"            
            self.doCalculations(dir,imglist,selectedfacesnum)
        else:
            print "Cache file exists"
	    f = open(cachefile)
            self.bundle=pickle.load(f)            
            oldlist=self.bundle.imglist            
            if(imglist==oldlist):
                print 'Both of the sets are same :D'
                cache_changed=False
                eigenfaces=self.bundle.eigenfaces
                adjfaces=self.bundle.adjfaces                             
                self.weights=self.calculateWeights(eigenfaces,adjfaces,selectedfacesnum);
		#print "EGfaces : ", len(eigenfaces), "   Adjfaces : ", len(adjfaces)
        	#print "Self.Weights : ", len(self.weights)

            if(cache_changed):
                print "Folder changed!!"                
                self.doCalculations(dir,imglist,selectedfacesnum)
            f.close()
	print "<... checkCache ENDS ....>"   
	'''	
            
