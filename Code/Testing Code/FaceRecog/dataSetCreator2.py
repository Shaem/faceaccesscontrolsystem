import cv2
import numpy as np
import Image

faceDetect =cv2.CascadeClassifier('haarcascade_frontalface_default.xml');
cam=cv2.VideoCapture(0);
id=raw_input('Enter User ID')
sampleNumber=0;
width = 15
height = 11

while(True):
    ret,img=cam.read();
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    faces=faceDetect.detectMultiScale(gray,1.3,5);
    for(x,y,w,h) in faces:
        sampleNumber=sampleNumber+1;

        ext = ".jpg"
        Imagename = "" +str(id) + "." + str(sampleNumber)+ ext
        ImageDirectroy = "dataSet/"
        print "Imagename : " + Imagename

        cv2.imwrite(ImageDirectroy + Imagename, gray[y:y+h,x:x+w])

        ImageFile = ImageDirectroy + Imagename
        image = Image.open(ImageFile)
        image = image.resize((width, height), Image.ANTIALIAS) 
        image.save(ImageFile)

        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        print "width : "+ str(w)
        print "Height : " + str(h)
        cv2.waitKey(1);
    cv2.imshow("Face",img)
    if(sampleNumber>30):
        break;
cam.release()
cv2.destroyAllWindows()
