import cv2
import numpy as np
from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
import argparse
import imutils
import dlib
import time
from PIL import Image

predictor_path = 'shape_predictor_68_face_landmarks.dat_2'
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(predictor_path)

faceDetect =cv2.CascadeClassifier('lbpcascade_frontalface.xml');
eyeDetect =cv2.CascadeClassifier('haarcascade_eye.xml');
cam=cv2.VideoCapture(0);
cam.set(3,112)
cam.set(4,92)
cam.set(5, .1)

p=0
q=0
count = 1
width = 92
height = 112
while(True):
    ret,img=cam.read();
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    faces=faceDetect.detectMultiScale(gray,1.3,5);

    for(x,y,w,h) in faces:

        ext = ".jpg"         
        Imagename = "Test" + ext
	ImageDirectroy = "database/"
        print "Imagename : " + Imagename
 
        cv2.imwrite(ImageDirectroy + Imagename, img[y:y+h,x:x+w])
        ImageFile = ImageDirectroy + Imagename
        
        image = Image.open(ImageFile)
        image = image.resize((width, height), Image.ANTIALIAS) 
        image.save(ImageFile)

	allignmentTimeStart = time.time()
        fa = FaceAligner(predictor)
        image = cv2.imread(ImageFile)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        rects = detector(gray, 2)

        ext = ".jpg"         
        Imagename = "FaceAllignTest" + ext
	ImageDirectroy = "FaceAllignTest/"
	img_path = ImageDirectroy+Imagename

        for rect in rects:
	    faceAligned = fa.align(image, gray, rect)
	    cv2.imwrite(img_path, faceAligned)
	    break

        allignmentTimeEnd = time.time()
        allignmentTimeDifference = allignmentTimeEnd - allignmentTimeStart
	print "Allignment Time : ", allignmentTimeDifference

        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        roi_gray=gray[y:y+h,x:x+w]
        roi_color=img[y:y+h,x:x+w]
        print "Face Width : "+str(w)
        print "Face Heigjt : "+str(h)

        # print "Width : "+str(p)
       # print "Height : "+str(q)    
        cv2.waitKey(1)
    cv2.imshow("Face",img);
    print "Width : "+str(p)
    print "Height : "+str(q)
    p=0
    q=0
    if(cv2.waitKey(1) == ord('q')):
        break;
cam.release()
cv2.destroyAllWindows()
